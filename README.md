Все действия выполнять с правами суперпользователя.

Создаём директорию и устанавливаем владельца: 
```
#!bash

cd /var/www && mkdir test-project && chown www-data:www-data test-project
```

Клонируем проект из репозитория в созданную папку:

```
#!bash

sudo -u www-data git clone https://intruder393@bitbucket.org/intruder393/test-project.git
```


Далее устанавливаем права на файлы и папки:
```
#!bash

find /var/www/test-project -type f -exec chmod 666 {} \;
find /var/www/test-project -type d -exec chmod 777 {} \;
```

Копируем конфиг:
```
#!bash

ln -s /var/www/test-project/bootstrap/test-project.conf /etc/apache2/sites-enabled
```

Добавляем строку в /etc/hosts
```
#!bash

127.0.0.1        test-project
```

Перезагружаем apache:
```
#!bash

service apache2 restart
```

В файле с константами необходимо указать параметры подключения к бд
```
#!bash

/var/www/test-project/app/helpers/constants.php
```
### Загружаем базу данных ###
Создаём новую базу данных:
```
#!bash

mysql> CREATE DATABASE magazin_db_b357;
```

Переходим в папку с проектом и непосредственно загружаем саму базу:

```
#!bash

cd /var/www/test-project
gunzip < magazin_db_b357.sql.gz | mysql -u root -p magazin_db_b357
```

http://test-project:8080/