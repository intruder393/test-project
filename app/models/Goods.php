<?php

class Goods {

    /**
     * Получаем список товаров по заданной категории
     * @param $id
     * @param int $page
     * @return array
     */
    public static function getListByCategory($id, $page = 1) {
        $result = array();
        //определяем лимит выдачи
        $page   = intval($page);
        $offset = ($page - 1) * SHOW_BY_DEFAULT;

        $db    = DB::getConnection();
        $query = "SELECT id, title, price, description, img FROM goods WHERE category_id=". $id
                ." ORDER BY id ASC "
                ." LIMIT ".SHOW_BY_DEFAULT
                ." OFFSET ".$offset;
        $list  = $db->query($query);
        $list->setFetchMode(PDO::FETCH_ASSOC);

        $i=0;
        while($row = $list->fetch()) {
            $result['items'][$i]['id']          = $row['id'];
            $result['items'][$i]['title']       = $row['title'];
            $result['items'][$i]['price']       = $row['price'];
            $result['items'][$i]['img']         = $row['img'];
            //$result[$i][''] = $row[''];
            $i++;
        }

        return $result;
    }

    public static function getList() {
        $result = array();

        $db    = DB::getConnection();
        $query = "SELECT id, title, price, description, img FROM goods "
            ." ORDER BY id ASC ";
        $list  = $db->query($query);
        $list->setFetchMode(PDO::FETCH_ASSOC);

        $i=0;
        while($row = $list->fetch()) {
            $result[$i]['id']          = $row['id'];
            $result[$i]['title']       = $row['title'];
            $result[$i]['price']       = $row['price'];
            //$result[$i][''] = $row[''];
            $i++;
        }

        return $result;
    }

    public static function getById($id = 1) {

        $db    = DB::getConnection();
        $query = "SELECT * FROM goods WHERE id=".$id." LIMIT 1";
        $list  = $db->query($query);
        return $list->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Получаем список товаров по указанным id
     * @param $idsArray
     * @return array
     */
    public static function getByIds($idsArray) {
        $goods     = array();
        $result    = array();
        $idsString = implode(',', $idsArray);

        $db    = DB::getConnection();
        $query = 'SELECT * FROM goods WHERE id IN ('.$idsString.')';
        $list  = $db->query($query);
        $list->setFetchMode(PDO::FETCH_ASSOC);

        $i=0;
        while($row = $list->fetch()) {
            $result[$i]['id']       = $row['id'];
            $result[$i]['title']    = $row['title'];
            $result[$i]['price']    = $row['price'];
            $result[$i]['img']      = $row['img'];
            $i++;
        }

        return $result;

    }

    /**
     * Удаляем товар по указонному id
     *
     * @param $id
     * @return bool
     */
    public static function deleteById($id) {
        $result = array();

        $db     = DB::getConnection();
        $query  = "DELETE FROM goods WHERE id = :id";
        $result = $db->prepare($query);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    /**
     * Добавляем новый товар
     * @param $options
     * @return int|string
     */
    public static function createGoods($options) {
        $result = array();

        $db = DB::getConnection();
        $query = 'INSERT INTO goods (category_id, brand_id, title, price, description, img, cpu, gpu, ram, hdd, status) '
               . ' VALUES (:category_id, :brand_id, :title, :price, :description, :img, :cpu, :gpu, :ram, :hdd, :status)';


        $result = $db->prepare($query);

        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':brand_id', $options['brand_id'], PDO::PARAM_INT);
        $result->bindParam(':title', $options['title'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':img', $options['img'], PDO::PARAM_STR);
        $result->bindParam(':cpu', $options['cpu'], PDO::PARAM_STR);
        $result->bindParam(':gpu', $options['gpu'], PDO::PARAM_STR);
        $result->bindParam(':ram', $options['ram'], PDO::PARAM_STR);
        $result->bindParam(':hdd', $options['hdd'], PDO::PARAM_STR);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);

        if ($result->execute()) {
            // Если запрос выполенен успешно, возвращаем id добавленной записи
            return $db->lastInsertId();
        }
        // Иначе возвращаем 0
        return 0;

    }

    /**
     *
     * Обновляем товар по заданному id
     * @param $id
     * @param $options
     * @return bool
     */
    public static function updateGoodsById($id, $options) {
        $result = array();

        $db = DB::getConnection();
        $query = 'UPDATE goods SET '
               .' category_id = :category_id,'
               .' brand_id = :brand_id,'
               .' title = :title,'
               .' price = :price,'
               .' description = :description,'
               .' cpu = :cpu,'
               .' gpu = :gpu,'
               .' ram = :ram,'
               .' hdd = :hdd,'
               .' status = :status'
                .' WHERE id = :id';

        $result = $db->prepare($query);

        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':brand_id', $options['brand_id'], PDO::PARAM_INT);
        $result->bindParam(':title', $options['title'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':cpu', $options['cpu'], PDO::PARAM_STR);
        $result->bindParam(':gpu', $options['gpu'], PDO::PARAM_STR);
        $result->bindParam(':ram', $options['ram'], PDO::PARAM_STR);
        $result->bindParam(':hdd', $options['hdd'], PDO::PARAM_STR);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        return $result->execute();
    }

    /**
     * Получаем количество товара по указанной категории
     *
     * @param $id
     * @return mixed
     */
    public static function getCount($id) {

        $db    = DB::getConnection();
        $query = "SELECT count(id) as count FROM goods WHERE category_id=".$id;
        $count = $db->query($query);
        return $count->fetch(PDO::FETCH_ASSOC);
    }
}