<?php

class Categories {

    public static function getCategoriesList() {
        $result = array();

        $db    = DB::getConnection();
        $query = "SELECT * FROM categories";
        $lists = $db->query($query);

        $i = 0;
        while ($row = $lists->fetch()) {
            $result[$i]['id']   = $row['id'];
            $result[$i]['name'] = $row['name'];
            $result[$i]['img']  = $row['img'];
            $i++;
        }
        return $result;
    }

    public static function getById($id) {
        $db    = DB::getConnection();
        $query = 'SELECT * FROM categories WHERE id='.$id;
        $item  = $db->query($query);
        return $item->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Удаляем категорию по id
     * @param $id
     * @return bool
     */
    public static function deleteById($id) {

        $db     = DB::getConnection();
        $query  = 'DELETE FROM categories WHERE id = :id';
        $result = $db->prepare($query);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }


    /**
     * Создаём новую категорию
     * @param $options
     * @return bool
     */
    public static function create($options) {
        $result = array();

        $db     = DB::getConnection();
        $query  = 'INSERT INTO categories (name) VALUES (:name)';
        $result = $db->prepare($query);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        return $result->execute();
    }

    /**
     * Обновляем указанную категорию
     * @param $id
     * @param $options
     * @return bool
     */
    public static function update($id, $options) {
        $result = array();

        $db     = DB::getConnection();
        $query  = 'UPDATE categories SET name = :name WHERE id = :id';
        $result = $db->prepare($query);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        return $result->execute();
    }


}