<?php

class Basket {

    /**
     * Сохраняем заказ в бд
     * @param $firstName
     * @param $phone
     * @param $goods
     * @return bool
     */
    public static function saveOrder($firstName, $phone, $goods) {
        $db    = DB::getConnection();
        $goods = json_encode($goods);

        $query = 'INSERT INTO basket (firstName, phone, goods) VALUES ('
               .':firstName, :phone, :goods )';

        $result = $db->prepare($query);
        $result->bindParam(':firstName', $firstName, PDO::PARAM_STR);
        $result->bindParam(':phone', $phone, PDO::PARAM_STR);
        $result->bindParam(':goods', $goods, PDO::PARAM_STR);
        return $result->execute();
    }

    public static function getList() {
        $result = array();

        $db = DB::getConnection();
        $query = 'SELECT * FROM basket';
        $list = $db->query($query);

        $i=0;
        while($row = $list->fetch(PDO::FETCH_ASSOC)) {
            $result[$i]['id']        = $row['id'];
            $result[$i]['firstName'] = $row['firstName'];
            $result[$i]['phone']     = $row['phone'];
            $result[$i]['goods']     = $row['goods'];
            $result[$i]['status']    = $row['status'];
        }
        return $result;
    }

    public static function deleteById($id) {
        $result = array();

        $db     = DB::getConnection();
        $query  = 'DELETE FROM basket WHERE id = :id';
        $result = $db->prepare($query);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function getById($id) {

        $db = DB::getConnection();
        $query = 'SELECT * FROM basket WHERE id='.$id;
        $item = $db->query($query);
        return $item->fetch(PDO::FETCH_ASSOC);
    }


    public static function updateById($id, $options) {
        $result = array();

        $db     = DB::getConnection();
        $query  = 'UPDATE basket SET firstName = :firstName, phone = :phone, status = :status '
                .' WHERE id = :id';
        $result = $db->prepare($query);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':firstName', $options['firstName'], PDO::PARAM_STR);
        $result->bindParam(':phone', $options['phone'], PDO::PARAM_STR);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        return $result->execute();
    }

    public static function getStatusText($status) {
        switch ($status) {
            case '1':
                return 'Новый заказ';
                break;
            case '2':
                return 'В обработке';
                break;
            case '3':
                return 'Доставляется';
                break;
            case '4':
                return 'Закрыт';
                break;
        }
    }

    /**
     * Очищаем корзину
     */
    public static function clear() {
        if (isset($_SESSION['goods'])) {
            unset($_SESSION['goods']);
        }
    }

    /**
     * Добавление товара в корзину (сессию)
     * @param $id
     */
    public static function addToSession($id) {
        $id = intval($id);
        $goodsInBasket = array();
        if (isset($_SESSION['goods'])) {
            $goodsInBasket = $_SESSION['goods'];
        }

        if (array_key_exists($id, $goodsInBasket)) {
            $goodsInBasket[$id]++;
        } else {
            $goodsInBasket[$id] = 1;
        }

        $_SESSION['goods'] = $goodsInBasket;
    }

    public static function deleteFromSession($id) {
        $id = intval($id);
        $goodsInBasket = array();
        if (isset($_SESSION['goods'])) {
            $goodsInBasket = $_SESSION['goods'];
        }

        if (array_key_exists($id, $goodsInBasket)) {
            unset($goodsInBasket[$id]);
        }
        $_SESSION['goods'] = $goodsInBasket;
    }

    public static function getFromSession() {
        if (isset($_SESSION['goods'])) {
            return $_SESSION['goods'];
        }
        return false;
    }

    /**
     * Общая цена товаров в корзине
     * @param $goods
     * @return int
     */
    public static function getTotalPrice($goods) {
        $goodsInBasket = self::getFromSession();

        $total = 0;

        if ($goodsInBasket) {
            foreach ($goods as $item) {
                $total += $item['price'] * $goodsInBasket[$item['id']];
            }
        }

        return $total;
    }

    /**
     * Количество товаров в корзине
     * @return int
     */
    public static function countItems() {
        if (isset($_SESSION['goods'])) {
            $count = 0;
            foreach ($_SESSION['goods'] as $id => $quantity) {
                $count += $quantity;
            }
            return $count;
        } else {
            return 0;
        }
    }

}