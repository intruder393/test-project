<?php

class Brands {

    /**
     * Получаем список производителей
     * @return array
     */
    public static function getBrands() {
        $result = array();

        $db    = DB::getConnection();
        $query = "SELECT * FROM brand";
        $lists = $db->query($query);

        $i = 0;
        while ($row = $lists->fetch()) {
            $result[$i]['id']   = $row['id'];
            $result[$i]['name'] = $row['name'];
            $i++;
        }
        return $result;
    }
}