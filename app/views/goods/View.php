<?php include ROOT_PATH."/views/layouts/header.php";?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Категории</h2>

                    <?php foreach($categoriesList as $item):?>
                        <a href="/category-<?php echo $item['id'];?>/page-1">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><?php echo $item['name'];?></h4>
                                </div>
                            </div>
                        </a>
                    <?php endforeach; ?>

                </div>
            </div>


            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2><?php echo $goodsItem['title'];?></h2>

                    <div class="tabs">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab-1" data-toggle="tab">Обзор</a></li>
                            <li><a href="#tab-2" data-toggle="tab">Характеристика</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab-1">
                                <div class="goods-item-characteristics">
                                    <div class="goods-item-img">
                                        <img src="../../public/images/shop<?php echo $goodsItem['img'];?>" alt="" />
                                    </div>
                                    <div class="goods-item-buy">
                                        <span class="goods-item-price"><?php echo $goodsItem['price'];?> грн</span>
                                    <span class="goods-item-btn-buy">
                                        <a href="/basket/g<?php echo $goodsItem['id'];?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Купить</a>
                                    </span>
                                    </div>
                                    <dl class="goods-characteristics">
                                        <div class="characteristics-tab">
                                            <dt class="characteristics-title">Краткие характеристики</dt>
                                            <dd class="characteristics-field"><?php echo $goodsItem['description'];?></dd>
                                        </div>
                                    </dl>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab-2">
                                <dl class="goods-characteristics">
                                   <div class="characteristics-tab">
                                       <dt class="characteristics-title">Краткие характеристики</dt>
                                       <dd class="characteristics-field"><?php echo $goodsItem['description'];?></dd>
                                   </div>

                                    <div class="characteristics-tab">
                                        <dt class="characteristics-title">Процессор</dt>
                                        <dd class="characteristics-field"><?php echo $goodsItem['cpu'];?></dd>
                                    </div>

                                    <div class="characteristics-tab">
                                        <dt class="characteristics-title">Графика</dt>
                                        <dd class="characteristics-field"><?php echo $goodsItem['gpu'];?></dd>
                                    </div>

                                    <div class="characteristics-tab">
                                        <dt class="characteristics-title">ОЗУ</dt>
                                        <dd class="characteristics-field"><?php echo $goodsItem['ram'];?></dd>
                                    </div>

                                    <div class="characteristics-tab">
                                        <dt class="characteristics-title">Объём накопителя</dt>
                                        <dd class="characteristics-field"><?php echo $goodsItem['hdd'];?></dd>
                                    </div>
                                </dl>
                            </div>
                        </div>
                    </div>

                </div><!--features_items-->

                <div class="category-tab"><!--category-tab-->

                </div><!--/category-tab-->

                <div class="recommended_items"><!--recommended_items-->

                </div><!--/recommended_items-->

            </div>
        </div>
    </div>
</section>

<?php include ROOT_PATH."/views/layouts/footer.php";?>