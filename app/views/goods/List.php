<?php include ROOT_PATH.'/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Категории</h2>

                    <?php foreach($categoriesList as $item):?>
                    <a href="/category-<?php echo $item['id'];?>/page-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo $item['name'];?></h4>
                        </div>
                    </div>
                        </a>
                    <?php endforeach; ?>

                </div>
            </div>


            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Товары</h2>

                    <?php if($goodsList && isset($goodsList['items'])) { foreach($goodsList['items'] as $item): ?>
                    <div class="col-sm-4 goods-item">
                        <div class="product-image-wrapper">
                            <div class="single-products goods">
                                <a href="/g<?php echo $item['id'];?>">
                                    <div class="productinfo text-center">
                                        <img src="../../public/images/shop<?php echo $item['img'];?>" alt="" />
                                        <h2><?php echo $item['price'];?> грн</h2>
                                        <p><?php echo $item['title'];?></p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>

                </div><!--features_items-->

                <div class="category-tab"><!--category-tab-->

                </div><!--/category-tab-->

                <div class="recommended_items"><!--recommended_items-->

                </div><!--/recommended_items-->

                <?php echo $pagination->get(); }?>

            </div>
        </div>
    </div>
</section>

<?php include ROOT_PATH.'/views/layouts/footer.php'?>