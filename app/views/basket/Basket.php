<?php include ROOT_PATH.'/views/layouts/header.php';?>



<section id="cart_items">
    <div class="container">
        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                <tr class="cart_menu">
                    <td class="image">Товар</td>
                    <td class="description">Заголовок</td>
                    <td class="price">Цена, шт</td>
                    <td class="count">Количество. шт</td>
                    <td class="total">Общая цена</td>
                    <td></td>
                </tr>
                </thead>
                <tbody>

                <?php if ($goodsInBasket) { foreach($goods as $item): ?>
                <tr>
                    <td class="cart_product">
                        <a href=""><img src="../../public/images/shop<?php echo $item['img'];?>" alt=""></a>
                    </td>
                    <td class="cart_description">
                        <h4><a href="/g<?php echo $item['id'];?>"><?php echo $item['title'];?></a></h4>
                        <p>ID: <?php echo $item['id'];?></p>
                    </td>
                    <td class="cart_price">
                        <p><?php echo $item['price'];?> грн</p>
                    </td>
                    <td class="cart_count">
                        <p><?php echo $goodsInBasket[$item['id']];?></p>
                    </td>
                    <td class="cart_total">
                        <p><?php echo $goodsInBasket[$item['id']] * $item['price'];?> грн</p>
                    </td>
                    <td class="cart_delete">
                        <a class="cart_quantity_delete" href="/basket/delete/g<?php echo $item['id'];?>"><i class="fa fa-times"></i></a>
                    </td>
                </tr>
                <?php endforeach; ?>
                    <tr class="basket-total-price">
                        <td colspan="4">
                            <a href="/basket/checkout" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Оформить заказ</a>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price"><?php echo $totalPrice;?> грн</p>
                        </td>
                        <td></td>
                    </tr>
                 <?php } else { ?>
                <tr>
                    <td colspan="6">Корзина пуста</td>
                </tr>
                <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</section> <!--/#cart_items-->

<?php include ROOT_PATH.'/views/layouts/footer.php';?>