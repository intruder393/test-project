<?php include ROOT_PATH.'/views/layouts/header.php';?>


    <section id="do_action">
        <div class="container">

            <div class="row">
                <div class="col-sm-3">

                </div>
                <div class="col-sm-6">
                    <div class="chose_area">
                        <form action="checkout" method="POST">
                        <?php if ($result): ?>
                            <p class="success-order">Заказ оформлкн. Мы вам перезвоним.</p>
                        <?php else: ?>
                            <div class="single_field user_order">
                                Выбрано товаров <?php echo $totalQuantity?>, на сумму <?php echo $totalPrice;?> грн.
                            </div>
                            <?php if(!$result): ?>
                                <?php if(isset($errors) && is_array($errors)): ?>
                                    <ul class="errors-order">
                                        <?php foreach($errors as $error):?>
                                            <li>- <?php echo $error;?></li>
                                        <?php endforeach;?>
                                    </ul>
                                <?php endif;?>
                                <div class="user_info">
                                    <div class="single_field first_name">
                                        <label>Имя:</label>
                                        <input type="text" name="firstName" value="<?php echo $userFirstName; ?>">
                                    </div>
                                    <div class="single_field user-phone">
                                        <label>Номер телефона:</label>
                                        <input type="text" name="phone" value="<?php echo $userPhone; ?>">
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-default buy" name="submit">Оформить</button>
                            <?php endif; ?>

                        <?php endif; ?>
                        </form>
                    </div>
                </div>
                <div class="col-sm-3">

                </div>
            </div>
        </div>
    </section><!--/#do_action-->


<?php include ROOT_PATH.'/views/layouts/footer.php';?>