<?php include ROOT_PATH."/views/layouts/header.php";?>

<section>
    <div class="container">
        <div class="row">

            <div class="col-sm-12 padding-center">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Категории</h2>

                    <?php foreach($categoriesList as $item): ?>
                    <div class="col-sm-3">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    <img src="../../public/images/categories/<?php echo $item['img'];?>" alt="" />
                                    <h2><?php echo $item['name'];?></h2>

                                </div>
                                <a href="/category-<?php echo $item['id'];?>/page-1" class="">
                                    <div class="product-overlay">
                                        <div class="overlay-content">
                                            <h2 class="overlay-h2-title"><?php echo $item['name'];?></h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>

                </div><!--features_items-->

                <div class="category-tab"><!--category-tab-->

                </div><!--/category-tab-->

                <div class="recommended_items"><!--recommended_items-->

                </div><!--/recommended_items-->

            </div>
        </div>
    </div>
</section>

<?php include ROOT_PATH."/views/layouts/footer.php";?>