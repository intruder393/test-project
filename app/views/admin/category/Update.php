<?php include ROOT_PATH . '/views/layouts/headerAdmin.php'; ?>

    <section>
        <div class="container">
            <div class="row">

                <br/>

                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/admin">Админпанель</a></li>
                        <li><a href="/admin/category">Управление категориями</a></li>
                        <li class="active">Редактировать категорию</li>
                    </ol>
                </div>


                <h4>Редактировать категорию "<?php echo $category['name']; ?>"</h4>

                <br/>

                <div class="col-lg-4">
                    <div class="login-form">
                        <form method="post">

                            <p>Название</p>
                            <input type="text" name="name" placeholder="" value="<?php echo $category['name']; ?>">

                            <input type="submit" name="submit" class="btn btn-default" value="Сохранить">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include ROOT_PATH . '/views/layouts/footerAdmin.php'; ?>