<?php include ROOT_PATH.'/views/layouts/headerAdmin.php';?>

    <section>
        <div class="container">
            <div class="row">

                <br/>

                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/admin">Админпанель</a></li>
                        <li class="active">Управление товарами</li>
                    </ol>
                </div>

                <a href="/admin/goods/create" class="btn btn-default back"><i class="fa fa-plus"></i> Добавить товар</a>

                <h4>Список товаров</h4>

                <br/>

                <table class="table-bordered table-striped table">
                    <tr>
                        <th>ID товара</th>
                        <th>Название товара</th>
                        <th>Цена</th>
                        <th></th>
                        <th></th>
                    </tr>
                    <?php foreach ($goodsList as $item): ?>
                        <tr>
                            <td><?php echo $item['id']; ?></td>
                            <td><?php echo $item['title']; ?></td>
                            <td><?php echo $item['price']; ?> грн</td>
                            <td><a href="/admin/goods/update/g<?php echo $item['id']; ?>" title="Редактировать"><i class="fa fa-pencil-square-o"></i></a></td>
                            <td><a href="/admin/goods/delete/g<?php echo $item['id']; ?>" title="Удалить"><i class="fa fa-times"></i></a></td>
                        </tr>
                    <?php endforeach; ?>
                </table>

            </div>
        </div>
    </section>

<?php include ROOT_PATH.'/views/layouts/footerAdmin.php';?>