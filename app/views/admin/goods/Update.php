<?php include ROOT_PATH . '/views/layouts/headerAdmin.php'; ?>

    <section>
        <div class="container">
            <div class="row">

                <br/>

                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/admin">Админпанель</a></li>
                        <li><a href="/admin/goods">Управление товарами</a></li>
                        <li class="active">Редактировать товар</li>
                    </ol>
                </div>


                <h4>Редактировать товар #<?php echo $id; ?></h4>

                <br/>

                <div class="col-lg-4">
                    <div class="login-form">
                        <form method="post" enctype="multipart/form-data">

                            <p>Название товара</p>
                            <input type="text" name="title" placeholder="" value="<?php echo $goodsItem['title'];?>">

                            <p>Стоимость, грн</p>
                            <input type="text" name="price" placeholder="" value="<?php echo $goodsItem['price'];?>">

                            <p>Категория</p>
                            <select name="category_id">
                                <?php if (is_array($categoriesList)): ?>
                                    <?php foreach ($categoriesList as $item): ?>
                                        <option value="<?php echo $item['id']; ?>"
                                            <?php if ($goodsItem['category_id'] == $item['id']) echo ' selected="selected"'; ?>>
                                            <?php echo $item['name']; ?>
                                        </option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <br/><br/>

                            <p>Производитель</p>
                            <select name="brand_id">
                                <?php if (is_array($brandsList)): ?>
                                    <?php foreach ($brandsList as $item): ?>
                                        <option value="<?php echo $item['id']; ?>"
                                            <?php if ($goodsItem['brand_id'] == $item['id']) echo ' selected="selected"'; ?>>
                                            <?php echo $item['name']; ?>
                                        </option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <br/><br/>

                            <p>Изображение товара</p>
                            <input type="file" name="image" placeholder="" value="">

                            <p>Краткие характеристики</p>
                            <textarea name="description" rows="5" cols="39"><?php echo $goodsItem['description'];?></textarea>

                            <br/><br/>

                            <br/><br/>

                            <p>Процессор</p>
                            <input type="text" name="cpu" placeholder="" value="<?php echo $goodsItem['cpu'];?>">

                            <p>Графика</p>
                            <input type="text" name="gpu" placeholder="" value="<?php echo $goodsItem['gpu'];?>">

                            <p>ОЗУ</p>
                            <input type="text" name="ram" placeholder="" value="<?php echo $goodsItem['ram'];?>">

                            <p>Объём накопителя</p>
                            <input type="text" name="hdd" placeholder="" value="<?php echo $goodsItem['hdd'];?>">

                            <br/><br/>

                            <p>Статус</p>
                            <select name="status">
                                <option value="1" selected="selected">Отображается</option>
                                <option value="0">Скрыт</option>
                            </select>

                            <br/><br/>

                            <input type="submit" name="submit" class="btn btn-default" value="Сохранить">

                            <br/><br/>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>

<?php include ROOT_PATH . '/views/layouts/footerAdmin.php'; ?>