<?php include ROOT_PATH . '/views/layouts/headerAdmin.php'; ?>

    <section>
        <div class="container">
            <div class="row">

                <br/>

                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/admin">Админпанель</a></li>
                        <li><a href="/admin/order">Управление заказами</a></li>
                        <li class="active">Просмотр заказа</li>
                    </ol>
                </div>


                <h4>Просмотр заказа #<?php echo $order['id']; ?></h4>
                <br/>




                <h5>Информация о заказе</h5>
                <table class="table-admin-small table-bordered table-striped table">
                    <tr>
                        <td>Номер заказа</td>
                        <td><?php echo $order['id']; ?></td>
                    </tr>
                    <tr>
                        <td>Имя клиента</td>
                        <td><?php echo $order['firstName']; ?></td>
                    </tr>
                    <tr>
                        <td>Телефон клиента</td>
                        <td><?php echo $order['phone']; ?></td>
                    </tr>
                    <tr>
                        <td><b>Статус заказа</b></td>
                        <td><?php echo Basket::getStatusText($order['status']); ?></td>
                    </tr>
                </table>

                <h5>Товары в заказе</h5>

                <table class="table-admin-medium table-bordered table-striped table ">
                    <tr>
                        <th>ID товара</th>
                        <th>Название</th>
                        <th>Цена</th>
                        <th>Количество</th>
                    </tr>
                    <?php foreach ($goodsList as $item): ?>
                        <tr>
                            <td><?php echo $item['id']; ?></td>
                            <td><?php echo $item['title']; ?></td>
                            <td><?php echo $item['price']; ?> грн</td>
                            <td><?php echo $goodsQuantity[$item['id']]; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>

                <a href="/admin/order/" class="btn btn-default back"><i class="fa fa-arrow-left"></i> Назад</a>
            </div>


    </section>

<?php include ROOT_PATH . '/views/layouts/footerAdmin.php'; ?>