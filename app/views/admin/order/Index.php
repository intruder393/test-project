<?php include ROOT_PATH . '/views/layouts/headerAdmin.php'; ?>

    <section>
        <div class="container">
            <div class="row">

                <br/>

                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/admin">Админпанель</a></li>
                        <li class="active">Управление заказами</li>
                    </ol>
                </div>

                <h4>Список заказов</h4>

                <br/>


                <table class="table-bordered table-striped table">
                    <tr>
                        <th>ID заказа</th>
                        <th>Имя покупателя</th>
                        <th>Телефон покупателя</th>
                        <th>Статус</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <?php foreach ($ordersList as $item): ?>
                        <tr>
                            <td>
                                <a href="/admin/order/view/g<?php echo $item['id']; ?>">
                                    <?php echo $item['id']; ?>
                                </a>
                            </td>
                            <td><?php echo $item['firstName']; ?></td>
                            <td><?php echo $item['phone']; ?></td>
                            <td><?php echo Basket::getStatusText($item['status']); ?></td>
                            <td><a href="/admin/order/view/g<?php echo $item['id']; ?>" title="Смотреть"><i class="fa fa-eye"></i></a></td>
                            <td><a href="/admin/order/update/g<?php echo $item['id']; ?>" title="Редактировать"><i class="fa fa-pencil-square-o"></i></a></td>
                            <td><a href="/admin/order/delete/g<?php echo $item['id']; ?>" title="Удалить"><i class="fa fa-times"></i></a></td>
                        </tr>
                    <?php endforeach; ?>
                </table>

            </div>
        </div>
    </section>

<?php include ROOT_PATH . '/views/layouts/footerAdmin.php'; ?>