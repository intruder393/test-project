<?php

class DB {

    /**
     * Подключение к бд
     * @return PDO
     */
    public static function getConnection() {
        $dsn = 'mysql:host='.DB_HOST_NAME.'; dbname='.DB_NAME;
        $db = new PDO($dsn, DB_USER_NAME, DB_PASSWORD);
        $db->query('SET NAMES utf8');
        return $db;
    }
}