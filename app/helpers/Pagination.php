<?php

class Pagination {

    private $max=5;
    private $index='page';
    private $currentPage;
    private $total;

    public function __construct($total, $currentPage, $index) {
        $this->total  = $total['count'];
        $this->index  = $index;
        $this->amount = $this->amount();
        $this->setCurrentPage($currentPage);
    }

    public function get() {
        $links = null;
        $limits = $this->limits();

        $html = "<div class=\"pagination-area\"><ul class=\"pagination\">";
        for($page=$limits[0]; $page<=$limits[1];$page++) {
            if($page == $this->currentPage) {
                $links .= "<li class=\"active\"><a href=\"\">".$page."</a></li>";
            } else {
                $links .= $this->generateHtml($page);
            }
        }

        if (!is_null($links)) {
            if ($this->currentPage > 1)
                // Создаём ссылку "На первую"
                $links = $this->generateHtml(1, '&lt;') . $links;
            // Если текущая страница не первая
            if ($this->currentPage < $this->amount)
                // Создаём ссылку "На последнюю"
                $links .= $this->generateHtml($this->amount, '&gt;');
        }

        $html .= $links . '</ul>';

        return $html;
    }

    private function generateHtml($page, $text = null) {
        // Если текст ссылки не указан
        if (!$text)
            // Указываем, что текст - цифра страницы
            $text = $page;
        $currentURI = rtrim($_SERVER['REQUEST_URI'], '/') . '/';
        $currentURI = preg_replace('~/page-[0-9]+~', '', $currentURI);


        return '<li><a href="' . $currentURI . $this->index . $page . '">' . $text . '</a></li>';
    }

    private function limits() {
        // Вычисляем ссылки слева (чтобы активная ссылка была посередине)
        $left = $this->currentPage - round($this->max / 2);

        // Вычисляем начало отсчёта
        $start = $left > 0 ? $left : 1;
        // Если впереди есть как минимум $this->max страниц
        if ($start + $this->max <= $this->amount) {
            // Назначаем конец цикла вперёд на $this->max страниц или просто на минимум
            $end = $start > 1 ? $start + $this->max : $this->max;
        } else {
            // Конец - общее количество страниц
            $end = $this->amount;
            // Начало - минус $this->max от конца
            $start = $this->amount - $this->max > 0 ? $this->amount - $this->max : 1;
        }

        return array($start, $end);
    }

    private function setCurrentPage($currentPage) {
        // Получаем номер страницы
        $this->currentPage = $currentPage;
        // Если текущая страница больше нуля
        if ($this->currentPage > 0) {
            // Если текущая страница меньше общего количества страниц
            if ($this->currentPage > $this->amount)
                // Устанавливаем страницу на последнюю
                $this->currentPage = $this->amount;
        } else
            // Устанавливаем страницу на первую
            $this->currentPage = 1;
    }

    private function amount() {
        return ceil($this->total / SHOW_BY_DEFAULT);
    }

}