<?php

define('ROOT_PATH', dirname(__DIR__));
define('ROUTES_PATH', ROOT_PATH . '/routers/routes.php');
define('CONTROLLERS_PATH', ROOT_PATH . '/controllers/');

define('DB_HOST_NAME', 'localhost');     //сервер
define('DB_USER_NAME', 'root'); 	    //пользователь
define('DB_PASSWORD', '');             //пароль
define('DB_NAME', 'magazin_db_b357'); //имя базы

define('SHOW_BY_DEFAULT', 9); //количество карточек на странице, для пагинации