<?php

class Router {

    private $routes;

    public function __construct() {
        $this->routes = include(ROUTES_PATH);
    }

    /**
     *
     * Return request string
     * @return string
     */
    private function getURI() {
        $uri = '/';
        if (!empty($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] !== '/') {
            $uri = trim($_SERVER['REQUEST_URI'], '/');
        }
        return $uri;
    }

    private function getRequestMethod() {
        return ( !empty($_SERVER['REQUEST_METHOD']) ) ? $_SERVER['REQUEST_METHOD'] : 'GET';

    }

    public function run() {
        $uri    = $this->getURI();
        $method = $this->getRequestMethod();

        //Проверить наличие такого запроса в routes.php
        foreach ($this->routes as $uriPattern => $path) {
            if (preg_match("~".$uriPattern."~", $uri)) {

                $internalRoute = preg_replace("~".$uriPattern."~", $path, $uri);

                //Определить контроллер
                $segments       = explode('/', $internalRoute);
                $controllerName = ucfirst( array_shift($segments) . 'Controller' );
                $actionName     = $method . ucfirst( array_shift($segments) );
                $options        = $segments;

                //Подключение файла класса контроллера
                $controllerFile = CONTROLLERS_PATH . $controllerName . '.php';

                if (file_exists($controllerFile)) {
                    include_once($controllerFile);
                }

                //Создание объекта и вызов необходимого метода
                $controllerObject = new $controllerName;
                $result = call_user_func_array( array($controllerObject, $actionName), $options );

                if ($result != null) {
                    break;
                }

            }
        }
    }
}