<?php

return array(
    //роуты корзины
    'basket/delete/g([0-9]+)' => 'basket/delete/$1',
    'basket/g([0-9]+)'        => 'basket/addToBasket/$1',
    'basket/checkout'         => 'basket/checkout',
    'basket'                  => 'basket/basketList',

    //роуты администратора
    'admin/goods/delete/g([0-9]+)' => 'adminGoods/delete/$1',
    'admin/goods/update/g([0-9]+)' => 'adminGoods/update/$1',
    'admin/goods/create'           => 'adminGoods/create',
    'admin/goods'                  => 'adminGoods/index',

    'admin/category/delete/g([0-9]+)' => 'adminCategory/delete/$1',
    'admin/category/update/g([0-9]+)' => 'adminCategory/update/$1',
    'admin/category/create'           => 'adminCategory/create',
    'admin/category'                  => 'adminCategory/index',

    'admin/order/delete/g([0-9]+)' => 'adminOrder/delete/$1',
    'admin/order/update/g([0-9]+)' => 'adminOrder/update/$1',
    'admin/order/view/g([0-9]+)'   => 'adminOrder/view/$1',
    'admin/order'                  => 'adminOrder/index',

    'admin' => 'admin/index',

    'g([0-9]+)' => 'goods/goodsById/$1 ',
    'category-([0-9]+)/page-([0-9]+)' => 'goods/goodsList/$1/$2 ',

    '/' => 'main/index',
);