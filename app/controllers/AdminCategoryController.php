<?php

class AdminCategoryController {

    public function GETIndex() {
        $categoriesList = Categories::getCategoriesList();

        require_once(ROOT_PATH.'/views/admin/category/Index.php');
        return true;
    }

    /**
     * Отображение вьюхи удаления
     * @param $id
     * @return bool
     */
    public function GETDelete($id) {

        require_once(ROOT_PATH.'/views/admin/category/Delete.php');
        return true;
    }

    /**
     *
     * Если был post запрос, удаляем категорию
     * @param $id
     * @return bool
     */
    public function POSTDelete($id) {
        if (isset($_POST['submit'])) {
            Categories::deleteById($id);
            header("Location: /admin/category");
            exit();
        }
        require_once(ROOT_PATH.'/views/admin/category/Delete.php');
        return true;
    }

    public function GETCreate() {

        require_once(ROOT_PATH.'/views/admin/category/Create.php');
        return true;
    }

    /**
     * Создаём категорию
     *
     * @return bool
     */
    public function POSTCreate() {

        if (isset($_POST['submit'])) {

            $options = array();

            $options['name'] = $_POST['categoryName'];

            $errors = false;

            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Укажите имя категории';
            }

            if ($errors == false) {
                Categories::create($options);
                header("Location: /admin/category");
                exit();
            }
        }

        require_once(ROOT_PATH.'/views/admin/category/Create.php');
        return true;
    }

    public function GETUpdate($id) {

        $category = Categories::getById($id);

        require_once(ROOT_PATH.'/views/admin/category/Update.php');
        return true;
    }

    /**
     * Обновляем категорию
     * @return bool
     */
    public function POSTUpdate($id) {

        if (isset($_POST['submit'])) {
            $options = array();

            $options['name'] = $_POST['name'];

            Categories::update($id, $options);
            header("Location: /admin/category");
            exit();
        }

        require_once(ROOT_PATH.'/views/admin/category/Update.php');
        return true;
    }
}