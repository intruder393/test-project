<?php

class GoodsController {

    /**
     * Выводим список товара по заданной категории
     *
     * @param $categoryId
     * @param int $page
     * @return bool
     */
    public function GETGoodsList($categoryId, $page = 1) {

        $goodsList      = Goods::getListByCategory($categoryId,$page);
        $categoriesList = Categories::getCategoriesList();
        $total          = Goods::getCount($categoryId);
        $pagination     = new Pagination($total, $page, 'page-') ;
        require_once(ROOT_PATH.'/views/goods/List.php');

        return true;
    }

    /**
     * Выводим страницу с товаром
     *
     * @param $id
     * @return bool
     */
    public function GETGoodsById($id) {

        $categoriesList = Categories::getCategoriesList();
        $goodsItem      = Goods::getById($id);
        require_once(ROOT_PATH.'/views/goods/View.php');

        return true;
    }
}