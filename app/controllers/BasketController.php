<?php

class BasketController {

    /**
     * Добавляем товар в корзину
     * с использованием сессии
     * @param $id
     */
    public function GETAddToBasket($id) {

        Basket::addToSession($id);
        $referer = $_SERVER['HTTP_REFERER'];
        header('Location: '.$referer);
        exit();
    }

    /**
     * Получаем и выводим информацию
     * о товарах в корзине
     * @return bool
     */
    public function GETBasketList() {

        $goodsInBasket = false;
        $goodsInBasket = Basket::getFromSession();

        if ($goodsInBasket) {
            $goodsIds   = array_keys($goodsInBasket);
            $goods      = Goods::getByIds($goodsIds);
            //Находим общую стоимость
            $totalPrice = Basket::getTotalPrice($goods);
        }

        require_once(ROOT_PATH.'/views/basket/Basket.php');

        return true;
    }

    public function GETDelete($id) {
        Basket::deleteFromSession($id);
        $referer = $_SERVER['HTTP_REFERER'];
        header('Location: '.$referer);
        exit();
    }

    public function GETCheckout() {
        //Получаем данные из корзины
        $goodsInBasket = false;
        $goodsInBasket = Basket::getFromSession();

        //Если нет товаров в корзине, отправляем выбирать на главную
        if ($goodsInBasket == false) {
            header('Location: /');
        }

        //Находим общую стоимость
        $goodsIds   = array_keys($goodsInBasket);
        $goods      = Goods::getByIds($goodsIds);
        $totalPrice = Basket::getTotalPrice($goods);

        //Количество товаров
        $totalQuantity = Basket::countItems();

        $userFirstName = false;
        $userPhone     = false;

        $result = false;

        require_once(ROOT_PATH.'/views/basket/Checkout.php');
        return true;
    }

    /**
     * Сохраняем заказ
     * @return bool
     */
    public function POSTCheckout() {

        if (isset($_POST['submit'])) {
            $userFirstName = $_POST['firstName'];
            $userPhone     = $_POST['phone'];

            //Получаем данные из корзины
            $goodsInBasket = false;
            $goodsInBasket = Basket::getFromSession();

            $errors = false;

            //Небольшая валидация
            if (empty($userFirstName)) {
                $errors[] = 'Неправильное имя';
            }
            if (empty($userPhone)) {
                $errors[] = 'Неверный телефон';
            }

            if ($errors == false) {
                //Если нет ошибок - сохраняемся
                $result = Basket::saveOrder($userFirstName, $userPhone, $goodsInBasket);

                if ($result) {
                    Basket::clear();
                }
            }

        }

        require_once(ROOT_PATH.'/views/basket/Checkout.php');
        return true;
    }
}