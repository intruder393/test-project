<?php

class AdminOrderController {

    /**
     * Главная страница заказов
     * @return bool
     */
    public function GETIndex() {

        $ordersList = Basket::getList();

        require_once(ROOT_PATH.'/views/admin/order/Index.php');
        return true;
    }

    public function GETDelete($id) {

        require_once(ROOT_PATH.'/views/admin/order/Delete.php');
        return true;
    }

    /**
     * Удаление заказа
     * @param $id
     * @return bool
     */
    public function POSTDelete($id) {

        if (isset($_POST['submit'])) {
            Basket::deleteById($id);
            header("Location: /admin/order");
            exit();
        }
        require_once(ROOT_PATH.'/views/admin/order/Delete.php');
        return true;
    }

    public function GETView($id) {

        $order         = Basket::getById($id);
        $goodsQuantity = json_decode($order['goods'], true);
        $goodsIds      = array_keys($goodsQuantity);

        $goodsList = Goods::getByIds($goodsIds);

        require_once(ROOT_PATH.'/views/admin/order/View.php');
        return true;
    }

    public function GETUpdate($id) {

        $order = Basket::getById($id);

        require_once(ROOT_PATH.'/views/admin/order/Update.php');
        return true;
    }

    public function POSTUpdate($id) {

        if (isset($_POST['submit'])) {
            $options = array();

            $options['firstName'] = $_POST['name'];
            $options['phone']     = $_POST['phone'];
            $options['status']    = $_POST['status'];

            Basket::updateById($id, $options);
            header('Location: /admin/order/view/g'.$id);
            exit();
        }

        require_once(ROOT_PATH.'/views/admin/order/Update.php');
        return true;
    }
}