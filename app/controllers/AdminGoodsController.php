<?php

class AdminGoodsController {

    public function GETIndex() {
        $goodsList = Goods::getList();

        require_once(ROOT_PATH.'/views/admin/goods/Index.php');
        return true;
    }

    public function GETCreate() {

        $categoriesList = Categories::getCategoriesList();
        $brandsList     = Brands::getBrands();

        require_once(ROOT_PATH. '/views/admin/goods/Create.php');
        return true;
    }

    /**
     * Добавление нового товара
     * @return bool
     */
    public function POSTCreate() {

        if (isset($_POST['submit'])) {
            // Получаем данные из формы
            $options['title']       = $_POST['title'];
            $options['price']       = $_POST['price'];
            $options['category_id'] = $_POST['category_id'];
            $options['brand_id']    = $_POST['brand_id'];
            //Валидацию
            $options['img']         = '/'.$_FILES['image']['name'];
            $options['description'] = $_POST['description'];
            $options['cpu']         = $_POST['cpu'];
            $options['gpu']         = $_POST['gpu'];
            $options['ram']         = $_POST['ram'];
            $options['hdd']         = $_POST['hdd'];
            $options['status']      = $_POST['status'];

            $id = Goods::createGoods($options);

            if ($id) {
                // Проверим, загружалось ли через форму изображение
                if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                    // Если загружалось, переместим его в нужную папке, дадим новое имя
                    move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/public/images/shop/".$_FILES['image']['name']);
                }
            };

            header("Location: /admin/goods");
            exit();

        }
        return true;
    }

    public function GETUpdate($id) {
        $categoriesList = Categories::getCategoriesList();
        $brandsList     = Brands::getBrands();

        $goodsItem = Goods::getById($id);

        require_once(ROOT_PATH.'/views/admin/goods/Update.php');
        return true;
    }

    /**
     * Обновление инофрмации о товаре
     * @param $id
     * @return bool
     */
    public function POSTUpdate($id) {

        if (isset($_POST['submit'])) {
            // Получаем данные из формы
            $options['title']       = $_POST['title'];
            $options['price']       = $_POST['price'];
            $options['category_id'] = $_POST['category_id'];
            $options['brand_id']    = $_POST['brand_id'];
            //Валидацию
            $options['img']         = '/'.$_FILES['image']['name'];
            $options['description'] = $_POST['description'];
            $options['cpu']         = $_POST['cpu'];
            $options['gpu']         = $_POST['gpu'];
            $options['ram']         = $_POST['ram'];
            $options['hdd']         = $_POST['hdd'];
            $options['status']      = $_POST['status'];

            $result = Goods::updateGoodsById($id, $options);

            header('Location: /admin/goods');
            exit();
        }

        return true;
    }

    public function GETDelete($id) {

        require_once(ROOT_PATH.'/views/admin/goods/Delete.php');
        return true;
    }

    /**
     * Удаление товара
     * @param $id
     * @return bool
     */
    public function POSTDelete($id) {

        if (isset($_POST['submit'])) {
            Goods::deleteById($id);
            header("Location: /admin/goods");
        }
        require_once(ROOT_PATH.'/views/admin/goods/Delete.php');
        return true;
    }

}