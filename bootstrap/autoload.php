<?php

session_start();

require_once('../app/helpers/constants.php');
require_once('../app/helpers/functions.php');

function __autoload($className) {
    $arrayPath = array(
        '/models/',
        '/helpers/',
        '/routers/'
    );

    foreach ($arrayPath as $path) {
        $path = ROOT_PATH . $path . $className . '.php';
        if (is_file($path)) {
            include_once($path);
        }
    }
}